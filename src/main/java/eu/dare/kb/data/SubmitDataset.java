package eu.dare.kb.data;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("submitdataset")
public class SubmitDataset {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String submitDataset(@QueryParam("token") String token,
                                @QueryParam("name") String name,
                                @QueryParam("desc") String description,
                                @QueryParam("component") String component,
                                @QueryParam("process") String process,
                                @QueryParam("workflow") String workflow,
                                @QueryParam("date") String created,
                                @QueryParam("path") String path){
        VirtuosoConnector connector = new VirtuosoConnector(null, "dare");
        connector.submitDataset(token, name, description, component, process, workflow, created, path);
        return null;
    }
}