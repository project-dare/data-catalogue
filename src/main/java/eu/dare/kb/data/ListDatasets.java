package eu.dare.kb.data;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("listdatasets")
public class ListDatasets {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String listDatasets(@QueryParam("user") String token){
        VirtuosoConnector connector = new VirtuosoConnector(null,"dare");
        connector.fetchDatasets(token);

        return "OK";
    }
}