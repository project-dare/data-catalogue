package eu.dare.kb.data;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.*;
import org.apache.jena.graph.Triple;
import org.apache.jena.graph.Node;

import java.util.UUID;

import virtuoso.jena.driver.*;

public class VirtuosoConnector {
    private String connectorURL;
    private VirtGraph graph;
    private static String baseData = "http://www.project-dare.eu/data/";
    private static String baseUser = "http://www.project-dare.eu/users/";

    public VirtuosoConnector(String url, String targetGraph){
        if (url == null)
            connectorURL = "jdbc:virtuoso://localhost:1111";
        else connectorURL = url;
        if (targetGraph == null)
            graph = new VirtGraph(connectorURL, "dba", "dba");
        else graph = new VirtGraph(targetGraph, connectorURL, "dba", "dba");

    }

    public String select(String query){
        Query sparql = QueryFactory.create(query);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, graph);
        ResultSet results = vqe.execSelect();
        int cnt = 0;
        while (results.hasNext()){
            cnt++;
            results.next();
        }
        System.out.println(cnt + " triples found");
        return null;
    }

    public String insert(String query){
        Node foo1 = NodeFactory.createURI("http://example.org/#foo1");
        Node bar1 = NodeFactory.createURI("http://example.org/#bar1");
        Node baz1 = NodeFactory.createURI("http://example.org/#baz1");

        UUID id = UUID.randomUUID();
        Node datasetNode = NodeFactory.createURI(baseData + UUID.randomUUID());
        Node nameNode = NodeFactory.createLiteral("sample dataset", "en");
        Node titleProp = NodeFactory.createURI("http://purl.org/dc/terms/title");



        VirtGraph newGraph = new VirtGraph ("Example3", connectorURL, "dba", "dba");

        newGraph.add(new Triple(datasetNode, titleProp, nameNode));
        System.out.println("graph.isEmpty() = " + newGraph.isEmpty());
        System.out.println("graph.getCount() = " + newGraph.getCount());
        //Query sparql = QueryFactory.create(query);
        //VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, graph);

        return null;
    }

    public boolean datasetExists(String name, String token){
        String query = "SELECT ?name ?token WHERE { ?d ?p ?o }";
        Query sparql = QueryFactory.create(query);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, graph);
        ResultSet results = vqe.execSelect();
        if (results.hasNext()) return true;
        else return false;
    }

    public void submitDataset(String token, String name, String description, String component, String process, String workflow, String created, String path){
        if (datasetExists(name, token)) return;
        UUID id = UUID.randomUUID();
        Node datasetNode = NodeFactory.createURI(baseData + UUID.randomUUID());
        Node typeNode = NodeFactory.createURI("http://www.project-dare.eu/ontology#Dataset");
        Node nameNode = NodeFactory.createLiteral(name, "en");
        Node idNode = NodeFactory.createLiteral(id.toString(), XSDDatatype.XSDstring);
        Node componentNode = NodeFactory.createLiteral(component, XSDDatatype.XSDstring);
        Node processNode = NodeFactory.createLiteral(process, XSDDatatype.XSDstring);
        Node workflowNode = NodeFactory.createLiteral(workflow, XSDDatatype.XSDstring);
        Node dateNode = NodeFactory.createLiteral(created, XSDDatatype.XSDdateTimeStamp);
        Node pathNode = NodeFactory.createLiteral(path,XSDDatatype.XSDstring);

        Node typeProp = NodeFactory.createURI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
        Node idProp = NodeFactory.createURI("http://purl.org/dc/terms/identifier");
        Node creatorProp = NodeFactory.createURI("http://www.project-dare.eu/ontology#user_token");
        Node titleProp = NodeFactory.createURI("http://purl.org/dc/terms/title");
        Node componentProp = NodeFactory.createURI("http://www.project-dare.eu/ontology#used_component");
        Node processProp = NodeFactory.createURI("http://www.project-dare.eu/ontology#via_process");
        Node workflowProp = NodeFactory.createURI("http://www.project-dare.eu/ontology#from_workflow");
        Node createdProp = NodeFactory.createURI("http://purl.org/dc/terms/created");
        Node pathProp = NodeFactory.createURI("http://www.project-dare.eu/ontology#local_path");

    }

    public void fetchDatasets(String token){
        String query = "SELECT ?id ?name ?description? ?date WHERE {?d <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.project-dare.eu/ontology#Dataset>}";
        return;
    }

    public void fetchDatasetInfo(String id){
        String query = "SELECT ?dataset WHERE {}";
        return;
    }

    public static void main(String args[]){
        VirtuosoConnector connector = new VirtuosoConnector(null, null);
        String query = "SELECT * WHERE { GRAPH ?graph { ?s ?p ?o } }";
        //connector.select(query);
        connector.insert(query);
    }
}